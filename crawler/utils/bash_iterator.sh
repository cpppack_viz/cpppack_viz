#!/bin/bash
set -e

progress_bar() {
  if [ "$1" == "done" ]; then
    spinner="X"
    percent_done="100"
    progress_message="Done!"
    new_line="\n"
  else
    spinner='/-\|'
    percent_done="${1:-0}"
    progress_message="$percent_done %"
  fi

  percent_none="$(( 100 - $percent_done ))"
  [ "$percent_done" -gt 0 ] && local done_bar="$(printf '#%.0s' $(seq -s ' ' 1 $percent_done))"
  [ "$percent_none" -gt 0 ] && local none_bar="$(printf '~%.0s' $(seq -s ' ' 1 $percent_none))"

  # print the progress bar to the screen
  printf "\r Progress: [%s%s] %s %s${new_line}" \
    "$done_bar" \
    "$none_bar" \
    "${spinner:x++%${#spinner}:1}" \
    "$progress_message"
}


function main() {

    cd tmp_data
    git clone https://github.com/Microsoft/vcpkg.git vcpkg_original

    rm -rf vcpkg || true
    cp -r vcpkg_original vcpkg
    cd vcpkg

    VCPKG_ROOT_FOLDER=$(pwd)
    # echo $VCPKG_ROOT_FOLDER

    counter=0
    # TODO get total number of commits first... and then show progress with progress bar...

    numerOfCommits=$(git rev-list --count master)
    maxNumberOfCommitsToEvaluate=50000

    dataFilePath="$VCPKG_ROOT_FOLDER/../../../src/assets/data.json"

    listOfCommits=$(git rev-list master)
    listOfCommitsReversed=$(printf '%s\n' "${listOfCommits[@]}" | tac | tr '\n' ' '; echo)

    echo "numerOfCommits: $numerOfCommits"
    echo '[' > $dataFilePath
    echo '  {' >> $dataFilePath
    echo '     "name": "# Packages", ' >> $dataFilePath
    echo '     "series": [' >> $dataFilePath

    for commit in $listOfCommitsReversed
    do
        # echo $commit
        echo "      {" >> $dataFilePath
        git checkout -f $commit --quiet

        cd ports
        echo "        \"name\": \"$(git show -s --format=%ci $commit)\"," >> $dataFilePath
        echo "        \"value\": $(find ./ -maxdepth 1 -type d | wc -l)" >> $dataFilePath
        # numberOfPackages="$(ls -p | grep -c /)" 
        
        cd $VCPKG_ROOT_FOLDER

        counter=$((counter+1))
        complete_size=$((100*$counter/$numerOfCommits))
        progress_bar "$complete_size"

        if [ "$counter" -gt "$maxNumberOfCommitsToEvaluate" ]; then
            echo "$maxNumberOfCommitsToEvaluate is greater than $counter";
            echo "      }" >> $dataFilePath
            break
        fi;
        
        echo '      },' >> $dataFilePath
        
    done
    # remove comma if there is one there...
    sed -i '$ s/},/}/g' $dataFilePath || true
    echo '    ]' >> $dataFilePath
    echo '  }' >> $dataFilePath
    echo ']' >> $dataFilePath
    git checkout master
    progress_bar "done"

}

main