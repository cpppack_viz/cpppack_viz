#!/bin/bash
set -e

function main() {

    cd tmp_data
    # git clone https://github.com/Microsoft/vcpkg.git vcpkg_original

    # rm -rf vcpkg || true
    # cp -r vcpkg_original vcpkg
    cd vcpkg

    VCPKG_ROOT_FOLDER=$(pwd)
    echo $VCPKG_ROOT_FOLDER
    cd ports

    dataFilePath="$VCPKG_ROOT_FOLDER/../../../src/assets/pkg_list.json"
    counter=0

    echo '[' > $dataFilePath

    for d in */ ; do
        echo '  {' >> $dataFilePath
        echo "     \"id\": \"$counter\"," >> $dataFilePath
        [[ "${d}" == */ ]] && d="${d: : -1}"
        echo "     \"pkgName\": \"$d\"," >> $dataFilePath
        echo "     \"source\": \"vcpkg\"" >> $dataFilePath
        echo '  },' >> $dataFilePath
        counter=$((counter+1))

    done

    # remove comma if there is one there...
    sed -i '$ s/},/}/g' $dataFilePath || true
    # echo '    ]' >> $dataFilePath
    echo ']' >> $dataFilePath

}

main