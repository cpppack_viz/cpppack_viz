#!/usr/bin/env python3

import json
import os
scriptPath = os.path.dirname(os.path.abspath(__file__))
portsDirectory = scriptPath + '/../tmp_data/vcpkg/ports'
pathToFile = scriptPath + '/../../src/assets/pkg_list.json'


def saveToFile(content, file):
    print('saving to file: ' + file)
    with open(pathToFile, 'w+') as outputFile:
        outputFile.write(content)
        outputFile.close()


def find_vcpkg_from_github(pathToportfile):
    datafile = open(pathToportfile)
    vcpkg_from_github_found = False
    cleanRepoURL = ""
    for line in datafile:
        if "vcpkg_from_github" in line:
            # print("vcpkg_from_github found for " + pathToportfile)
            vcpkg_from_github_found = True
        if vcpkg_from_github_found:
            if "REPO" in line:
                # print("REPO found for " + pathToportfile)
                cleanRepoURL = line.replace('REPO', '')
                cleanRepoURL = cleanRepoURL.replace(' ', '')
                cleanRepoURL = "https://github.com/" + cleanRepoURL
                cleanRepoURL = cleanRepoURL.replace("\n", '')
                # print(cleanRepoURL)

    return cleanRepoURL


def vcpkg_download_distfile(pathToportfile):
    datafile = open(pathToportfile)
    vcpkg_download_distfile_found = False
    cleanRepoURL = ""
    for line in datafile:
        if "vcpkg_download_distfile" in line:
            print("vcpkg_download_distfile found for " + pathToportfile)
            vcpkg_download_distfile_found = True
        if vcpkg_download_distfile_found:
            if "URLS" in line:
                print("URLS found for " + pathToportfile)
                cleanRepoURL = line.replace('URLS', '')
                cleanRepoURL = cleanRepoURL.replace(' ', '')
                cleanRepoURL = cleanRepoURL.replace('"', '')
                cleanRepoURL = cleanRepoURL.replace("\n", '')
                print(cleanRepoURL)

    return cleanRepoURL


def find_vcpkg_description_in_control_file(pathToControlfile):
    datafile = open(pathToControlfile)
    description = ""
    for line in datafile:
        if "Description:" in line:
            description = line
            description = description.replace("\n", '')
            description = description.replace("Description: ", '')

    return description


def find_vcpkg_build_depends_in_control_file(pathToControlfile):
    datafile = open(pathToControlfile)
    buildDepends = ""
    for line in datafile:
        if "Build-Depends:" in line:
            buildDepends = line
            buildDepends = buildDepends.replace("\n", '')
            buildDepends = buildDepends.replace("Build-Depends: ", '')

    return buildDepends


def getUpstreamURLVcpkg(vcpkgDirectory):
    pathToportfile = portsDirectory + "/" + vcpkgDirectory + "/portfile.cmake"
    # print(pathToportfile)
    githubURL = find_vcpkg_from_github(pathToportfile)
    if githubURL:
        return githubURL

    upstreamURL = vcpkg_download_distfile(pathToportfile)
    if upstreamURL:
        return upstreamURL

    return ""


def getDescriptionVcpkg(vcpkgDirectory):
    pathToControlfile = portsDirectory + "/" + vcpkgDirectory + "/CONTROL"
    return find_vcpkg_description_in_control_file(pathToControlfile)


def getBuildDependsVcpkg(vcpkgDirectory):
    pathToControlfile = portsDirectory + "/" + vcpkgDirectory + "/CONTROL"
    my_string = find_vcpkg_build_depends_in_control_file(pathToControlfile)
    result = [x.strip() for x in my_string.split(',')]
    print(result)
    return result


if __name__ == '__main__':
    folders = list(filter(lambda x: os.path.isdir(
        os.path.join(portsDirectory, x)), os.listdir(portsDirectory)))
    # print(scriptPath)

    jsonArray = []
    counter = 0
    for pkg in folders:
        upstreamURL = getUpstreamURLVcpkg(pkg)
        description = getDescriptionVcpkg(pkg)
        buildDepends = getBuildDependsVcpkg(pkg)
        # print(upstreamURL)
        # print(pkg)
        x = {
            "id": counter,
            "pkgName": pkg,
            "source": "vcpkg",
            "upstream": upstreamURL,
            "description": description,
            "buildDepends": buildDepends
        }
        jsonArray.append(x)
        counter = counter + 1

    y = json.dumps(jsonArray, indent=4, sort_keys=True)
    # print(y)

    saveToFile(y, pathToFile)
