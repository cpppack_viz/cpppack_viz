#!/usr/bin/env python3

import json

pathToFile = './../src/assets/data.json'

if __name__ == '__main__':
    with open(pathToFile, 'r') as inputFile:
        rawData = inputFile.read()
        # json_validator(rawData)
        try:
            json.loads(rawData)
            print("json seems to be ok")
            exit(0)
        except ValueError as error:
            print("invalid json: %s" % error)
            exit(1)
