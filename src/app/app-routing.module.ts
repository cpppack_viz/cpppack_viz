import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BrowserComponent } from './pages/browser/browser.component';
import { VcpkgComponent } from './pages/vcpkg/vcpkg.component';
import { ConanComponent } from './pages/conan/conan.component';

const routes: Routes = [
  {
    path: 'browser',
    component: BrowserComponent
  },
  {
    path: 'vcpkg',
    component: VcpkgComponent
  },
  {
    path: 'conan',
    component: ConanComponent
  },
  { path: '', redirectTo: '/browser', pathMatch: 'full' },
  { path: '**', redirectTo: '/browser', pathMatch: 'full' }
];


@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
