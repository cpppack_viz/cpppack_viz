import { Component } from '@angular/core';
import { AppConstants } from './app.constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cpppack-viz';
  version = AppConstants.version;
  hash = AppConstants.hash;
  lastUpdate = AppConstants.lastUpdate;

  constructor() {
  }

}
