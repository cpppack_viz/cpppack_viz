import { environment } from '../environments/environment';

export class AppConstants {
  public static version = '0.0.1';
  public static hash = 'UNKNOWN';
  public static lastUpdate = 'BUILD_DATE';
  public static production = environment.production;
}
