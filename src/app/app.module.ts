import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AlertModule } from 'ngx-bootstrap/alert';
import { AppRoutingModule } from './app-routing.module';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';

import { AppComponent } from './app.component';
import { BrowserComponent } from './pages/browser/browser.component';
import { VcpkgComponent } from './pages/vcpkg/vcpkg.component';
import { ConanComponent } from './pages/conan/conan.component';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { HighlightModule } from 'ngx-highlightjs';
import { ClipboardModule } from 'ngx-clipboard';

import bash from 'highlight.js/lib/languages/bash';

export function hljsLanguages() {
  return [
    {name: 'bash', func: bash},
  ];
}

@NgModule({
  declarations: [
    AppComponent,
    BrowserComponent,
    VcpkgComponent,
    ConanComponent
  ],
  imports: [
    BrowserModule,
    NgxChartsModule,
    FormsModule,
    BrowserAnimationsModule,
    AlertModule.forRoot(),
    AppRoutingModule,
    TypeaheadModule.forRoot(),
    FilterPipeModule,
    AccordionModule.forRoot(),
    HighlightModule.forRoot({
      languages: hljsLanguages
    }),
    ClipboardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
