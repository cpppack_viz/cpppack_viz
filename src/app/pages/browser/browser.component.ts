import { Component } from '@angular/core';
import * as packages from './../../../assets/pkg_list.json';
import { FilterPipe } from 'ngx-filter-pipe';

@Component({
  selector: 'app-browser-root',
  templateUrl: './browser.component.html',
  styleUrls: ['./browser.component.scss']
})
export class BrowserComponent {
  packages: any[];

  pkgFilter: any = { pkgName: '' };

  constructor(private filterPipe: FilterPipe) {
    this.packages = packages.default;
  }
}
