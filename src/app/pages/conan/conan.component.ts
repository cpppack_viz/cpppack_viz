import { Component, NgZone } from '@angular/core';
// import * as am4core from '@amcharts/amcharts4/core';
// import * as am4charts from '@amcharts/amcharts4/charts';
// import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { AppConstants } from './../../app.constants';
// import * as lineData from './../../../assets/data.json';

// am4core.useTheme(am4themes_animated);

@Component({
  selector: 'app-connan-root',
  templateUrl: './conan.component.html',
  styleUrls: ['./conan.component.scss']
})
export class ConanComponent {
  // private chart: am4charts.XYChart;
  version = AppConstants.version;
  hash = AppConstants.hash;
  lastUpdate = AppConstants.lastUpdate;
  lineData: any[];

  constructor(private zone: NgZone) {
    // this.lineData = lineData.default;
  }

}
