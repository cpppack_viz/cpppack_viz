import { Component, NgZone, AfterViewInit, OnDestroy } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { AppConstants } from './../../app.constants';
import * as lineData from './../../../assets/data.json';

am4core.useTheme(am4themes_animated);

@Component({
  selector: 'app-vcpkg-root',
  templateUrl: './vcpkg.component.html',
  styleUrls: ['./vcpkg.component.scss']
})
export class VcpkgComponent implements AfterViewInit, OnDestroy {
  title = 'cpppack-viz';
  private chart: am4charts.XYChart;
  lineData: any[];

  constructor(private zone: NgZone) {
    this.lineData = lineData.default;
  }

  strToDate(str1: string): Date {
    const yr1 = parseInt(str1.substring(0, 4), 10);
    const mon1 = parseInt(str1.substring(5, 7), 10);
    const dt1 = parseInt(str1.substring(8, 10), 10);
    let hourst1 = parseInt(str1.substring(11, 13), 10);
    const mint1 = parseInt(str1.substring(14, 16), 10);
    const sect1 = parseInt(str1.substring(17, 19), 10);
    const offset = parseInt(str1.substring(21, 23), 10);
    const diff = str1.substring(20, 21);

    if (diff === '-') {
      hourst1 = hourst1 - offset;
    } else if (diff === '+') {
      hourst1 = hourst1 + offset;
    }

    const date1 = new Date(yr1, mon1 - 1, dt1, hourst1, mint1, sect1);
    return date1;
  }

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      const chart = am4core.create('chartdiv', am4charts.XYChart);

      chart.paddingRight = 20;

      const data = [];
      for (let i = 0; i < this.lineData[0].series.length; i++) {
        const date = this.strToDate(this.lineData[0].series[i].name);
        data.push({ date: date, name: 'name' + i, value: this.lineData[0].series[i].value });
      }

      chart.data = data;

      const dateAxis = chart.xAxes.push(new am4charts.DateAxis());
      dateAxis.renderer.grid.template.location = 0;

      const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.tooltip.disabled = true;
      valueAxis.renderer.minWidth = 35;

      const series = chart.series.push(new am4charts.LineSeries());
      series.dataFields.dateX = 'date';
      series.dataFields.valueY = 'value';

      series.tooltipText = '{valueY.value}';
      chart.cursor = new am4charts.XYCursor();

      const scrollbarX = new am4charts.XYChartScrollbar();
      scrollbarX.series.push(series);
      chart.scrollbarX = scrollbarX;

      this.chart = chart;
    });
  }

  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
}
